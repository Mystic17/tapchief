export const imdbReducer =(state ={} , action) =>{    
    switch(action.type){
        case 'IMDB_STARTED':
            return Object.assign({}, state);
        case 'IMDB_SUCCESS':
            return Object.assign({}, state, {data: action.data});
        case 'IMDB_ERROR':
            return Object.assign({}, state);
        default: 
            return state;
    }
}
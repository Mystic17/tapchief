export const upcomingListReducer =(state ={} , action) =>{    
    switch(action.type){
        case 'UPCOMING_LIST_STARTED':
            return Object.assign({}, state);
        case 'UPCOMING_LIST_SUCCESS':
            return Object.assign({}, state, {data: action.data});
        case 'UPCOMING_LIST_ERROR':
            return Object.assign({}, state);
        default: 
            return state;
    }
}
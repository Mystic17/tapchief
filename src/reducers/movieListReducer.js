export const movieListReducer =(state ={} , action) =>{    
    switch(action.type){
        case 'LIST_STARTED':
            return Object.assign({}, state);
        case 'LIST_SUCCESS':
            return Object.assign({}, state, {data: action.data});
        case 'LIST_ERROR':
            return Object.assign({}, state);
        default: 
            return state;
    }
}
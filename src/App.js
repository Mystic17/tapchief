import React, { Component } from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Upcoming from './components/Upcoming';
import {connect} from 'react-redux';
import {movieListThunk} from './actions/movieListAction';
import Imdb from './components/Imdb';

class App extends Component {

  constructor(props){
    super(props);
    this.state={
      title:''
    }
    this.handleChange = this.handleChange.bind(this);
    this.callAPi = this.callAPi.bind(this);
    // this.details = this.details.bind(this);
  }

  handleChange(e){
    e.preventDefault();
    this.setState({
      title: e.target.value
    });

  }
  callAPi(){
    //dispatch an action
    this.props.dispatch(movieListThunk(this.state.title));
  }

  render() {
    
    if(this.props.movieListReducer.data){
      

    var postList= this.props.movieListReducer.data.map((e,i) =>{

      var castList=[];
      var castList1 = [];
      //printing only 3 cast members
      console.log("Data :", e.credits.cast);
      console.log("Data Length :", e.credits.cast.length);

     if (e.credits.cast.length > 2){
      for(let j=0;j<=2;j++){     
        castList[j] = e.credits.cast[j].name + ',';
    }
     }
     else{
       castList[0] = "NIL"
     }

    //  castList.join('');

      return (
        <div key={i}>
        <table className = 'table1'>
          <thead>
          <tr>
            <th>Title</th>
            <th>Date of Release</th>
            <th>Cast</th>
          </tr>
          </thead>
          <tbody>
            <tr>
            <td>{e.title}</td>
            <td>{e.release_date}</td>
            <td>{castList}</td>
            <td><a href={'/imdb?id='+ e.imdb_id}>Details</a></td>
          </tr>
          </tbody>
        </table>
        </div>
      );
      });       
      
      }

    return (
      <Router>
      <div className="App">
        <label>Title: </label>
        <input type= "text" className="input-small" name="t" placeholder="Enter keyword.." onChange={this.handleChange} value={this.state.title}/>
        <button type= "button" className ="btn btn-primary button1" onClick={this.callAPi}> Search</button>
        <br/>
        <a href='/upcoming'>Upcoming</a>
        {postList}
        <Route exact path='/upcoming' component={Upcoming}/>
        <Route path ='/imdb' component ={Imdb}/>
        

      </div>
      </Router>
    );
  }
}
const mapStateToProps = (state) =>{
  return state;
}

export default connect(mapStateToProps)(App);
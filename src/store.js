import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import {movieListReducer} from './reducers/movieListReducer';
import {upcomingListReducer} from './reducers/upcomingListReducer';
import {imdbReducer} from './reducers/imdbReducer';

const store = createStore(combineReducers({movieListReducer, upcomingListReducer, imdbReducer}), applyMiddleware(thunk));

export default store;
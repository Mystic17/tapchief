import axios from 'axios';

const imdb_Started = () =>{
    return({
        type : 'IMDB_STARTED'
    });
}

const imdb_Success = (data) =>{
    return({
        type: 'IMDB_SUCCESS',
        data: data
    })
}

const imdb_Error = () =>{
    return({
        type: 'IMDB_ERROR'
    })
}

export const imdbThunk = (input)=>{
    return dispatch =>{
        dispatch(imdb_Started);
        axios.get('https://api.themoviedb.org/3/find/'+input+'?api_key=11bcf090ce5dc79be534e64d1e57c1c9&external_source=imdb_id').then((res) =>{
            console.log(res.data.movie_results[0]);
            dispatch(imdb_Success(res.data.movie_results[0]));
        }) .catch((err)=>{
            console.log(err);
            dispatch(imdb_Error);
        })
    }
}
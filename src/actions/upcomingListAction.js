import axios from 'axios';

const upcoming_List_Started = () =>{
    return({
        type : 'UPCOMING_LIST_STARTED'
    });
}

const upcoming_List_Success = (data) =>{
    return({
        type: 'UPCOMING_LIST_SUCCESS',
        data: data
    })
}

const upcoming_List_Error = () =>{
    return({
        type: 'UPCOMING_LIST_ERROR'
    })
}

export const upcomingThunk = (input)=>{
    return dispatch =>{
        dispatch(upcoming_List_Started);
        axios.get('http://api.themoviedb.org/3/movie/upcoming?api_key=11bcf090ce5dc79be534e64d1e57c1c9').then((res)=>{
            console.log(res.data.results);
            var arr=[];
            for(let i=0;i<res.data.results.length;i++){
                arr[i] = res.data.results[i].title;
            }
            dispatch(upcoming_List_Success(arr));

        }).catch((err)=>{
            console.log(err);
            dispatch(upcoming_List_Error);
        });
    }
}
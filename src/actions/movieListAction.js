import axios from 'axios';

const movie_List_Started = () =>{
    return({
        type : 'LIST_STARTED'
    });
}

const movie_List_Success = (data) =>{
    return({
        type: 'LIST_SUCCESS',
        data: data
    })
}

const movie_List_Error = () =>{
    return({
        type: 'LIST_ERROR'
    })
}

export const movieListThunk = (input) =>{
    return dispatch =>{
        dispatch(movie_List_Started);
        console.log(localStorage.getItem('title'));

      if(localStorage.getItem('title') === input){  
            console.log('No API call');
            dispatch(movie_List_Success(JSON.parse(localStorage.getItem('movie'))));
      }
        else{
      console.log('Making API call');
      //Api call to receive other details
      axios.get('https://api.themoviedb.org/3/search/movie?query='+input+'&api_key=11bcf090ce5dc79be534e64d1e57c1c9').then((res)=>{
      var idList=[],results =[];
      for(let i=0;i<res.data.results.length;i++){
        idList[i] = res.data.results[i].id;
      }
      results = res.data.results;
      localStorage.setItem('title', input);

      var idArr = [],arrList=[],imdbList=[],response=[];
      
      //api call to receive cast using tmdb id
      for(let i = 0; i<results.length;i++){
        idArr[i] = results[i].id;
      axios.get('https://api.themoviedb.org/3/movie/'+idArr[i]+'?api_key=11bcf090ce5dc79be534e64d1e57c1c9&append_to_response=credits').then((res)=>{
     console.log(res.data);
     response[i] = res.data
     imdbList[i] = res.data.imdb_id
      arrList[i] = res.data.credits.cast;
    
      localStorage.setItem('movie', JSON.stringify(response));
      dispatch(movie_List_Success(response));
      
    }).catch((err) =>{
      console.log(err);
      dispatch(movie_List_Error);
    });
  }
    }).catch((err)=>{
      console.log(err);
      dispatch(movie_List_Error);
    });
    }
}
}
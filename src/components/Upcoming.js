import React from 'react';
import {withRouter} from 'react-router-dom';
import '../App.css';
import {connect} from 'react-redux';
import {upcomingThunk} from '../actions/upcomingListAction';
import {keywordAscThunk} from '../actions/keywordAscAction';
import {keywordDescThunk} from '../actions/keywordDescAction';
import {yearDescThunk} from '../actions/yearDescAction';
import {yearAscThunk} from '../actions/yearAscAction';

class Upcoming extends React.Component{
    constructor(props){
        super(props);
         this.keywordAsc = this.keywordAsc.bind(this);
         this.keywordDesc = this.keywordDesc.bind(this);
         this.yearAsc = this.yearAsc.bind(this);
         this.yearDesc = this.yearDesc.bind(this);
    }
    keywordAsc(){  
        this.props.dispatch(keywordAscThunk());
    }
    keywordDesc(){
        this.props.dispatch(keywordDescThunk());
    }
    yearAsc(){
        this.props.dispatch(yearAscThunk());
    }
    yearDesc(){
        this.props.dispatch(yearDescThunk());
    }
    
    componentDidMount(){
        this.props.dispatch(upcomingThunk());
    }
    
    render(){
       
        if(this.props.upcomingListReducer.data){
            
            var upcomingList = this.props.upcomingListReducer.data.map((e,i) =>{
                        return(
                            <div key={i} className = 'uplist'>
                                <h4>{e}</h4>
                            </div>
                        );
                    }) 
        }

        return(
            <div>                
                <button onClick={this.keywordAsc} className = "btn btn-primary but">Keyword <i className="fa fa-arrow-up" aria-hidden="true"></i></button>
                <button onClick={this.keywordDesc} className = "btn btn-primary but">Keyword <i className="fa fa-arrow-down" aria-hidden="true"></i></button>
                <button onClick={this.yearAsc} className = "btn btn-primary but">Year <i className="fa fa-arrow-up" aria-hidden="true"></i></button>
                <button onClick={this.yearDesc} className = "btn btn-primary but">Year <i className="fa fa-arrow-down" aria-hidden="true"></i></button>
                <h3 className="heading">Upcoming movies</h3>
                {upcomingList}      
            </div>
        );
}
}

const mapStateToProps = (state) =>{
    return state;
}

export default withRouter(connect(mapStateToProps)(Upcoming));
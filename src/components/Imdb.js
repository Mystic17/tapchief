import React from 'react';
import queryString from 'query-string';
import {imdbThunk} from '../actions/imdbAction';
import {connect} from 'react-redux';
import '../App.css';

class Imdb extends React.Component{
    componentDidMount(){
        let parsed = queryString.parse(this.props.location.search);
        console.log(parsed.id);
        this.props.dispatch(imdbThunk(parsed.id));
    }
    render(){
        if(this.props.imdbReducer.data){
            return(
            <div className = "imdbData">
                <h3><strong>{this.props.imdbReducer.data.title}</strong></h3>
                <h4>({this.props.imdbReducer.data.original_title})</h4>
                <h5>Popularity: {this.props.imdbReducer.data.popularity}</h5>
                <h5>Release date: {this.props.imdbReducer.data.release_date}</h5>
                <p style={{fontSize: 20}}>Overview: {this.props.imdbReducer.data.overview}</p>
            </div>
        );
    }else{
        return(
                <div>Loading</div>
        );
    }
}
}

const mapStateToProps = (state) =>{
    return state;
}

export default connect(mapStateToProps)(Imdb);